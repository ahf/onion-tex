%% Copyright (c) 2018 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,17pt]{beamer}

\usepackage{soul,comment}
\usepackage{bbding,enumitem}

\usepackage{tikzpeople}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{ulem}

%\usepackage[export]{adjustbox}

\usetheme{onion}

\title{Lessons learned running Tor exit relays at an NREN}

%\date{April 7 2023}
\date{}
\author{Roger Dingledine (The Tor Project)\\Jessica Schumacher (Switch)}
%\institute{The Tor Project}
%\vspace{-1cm}
\titlegraphic{\onionlogo{OnionBlack}}

\setlength{\parskip}{3mm}

% put page numbers on each slide
\setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

%% AHF: \tikzexternalize uses the external library to cache tikz images, but needs --shell-escape to be passed to pdflatex.
%\usetikzlibrary{shapes,backgrounds,dateplot,external}
\usetikzlibrary{shapes,backgrounds,dateplot,external,lindenmayersystems}
%%\tikzexternalize[prefix=tikz/]

\newcommand{\highlight}[1]{\textbf{\alert{#1}}}

\newcommand{\colorhref}[3][OnionBlack]{\href{#2}{\color{#1}{#3}} }

\begin{document}

\maketitle

\begin{frame}{Daily users of the Tor Snowflake transport}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{../arma-aisa-2024/images/snowflake-users-russia.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{../arma-iu-2023/images/snowflake-big-picture.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.6\textwidth]{../arma-37c3-2023/iran-images/screenshot-twitter-iran.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{../arma-37c3-2023/iran-images/screenshot-google-app-store-iran.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{images/screenshot-saarland-de.png}
\end{figure}
\end{frame}
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{images/screenshot-saarland-en.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{images/screenshot-freiburg-de.png}
\end{figure}
\end{frame}
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{images/screenshot-freiburg-en.png}
\end{figure}
\end{frame}

\begin{frame}{Snowflake volunteers}
\vspace{-1cm}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{../arma-37c3-2023/iran-images/snowflake-proxy-type.png}
\end{figure}
\end{frame}

\begin{frame}{Outline}

\begin{itemize}
\item (0) Motivation / inspiration
\item {\bf{(1) Intro to Tor and our values}}
\item (2) Connection to educational missions and NRENs
%\item (3) Tor relays in European NRENs already
\item (3) Policy decisions you might face too
\end{itemize}

\end{frame}

\begin{frame}{Tor Overview}

%Tor provides
Online anonymity: open source, open network

Community of devs, researchers, users, relay operators

US 501(c)(3) non-profit organization with 50ish staff

Estimated 2,000,000 to 8,000,000 daily users

Part of larger ecosystems: internet freedom, free software, censorship
resistance, anonymity research

\end{frame}

%\begin{frame}{Tor Browser}
%\begin{figure}[htbp]
%\includegraphics[width=\linewidth]{../arma-aisa-2024/images/130-homepage.png}
%\end{figure}
%\end{frame}

%\input{threat-model.tex}

\begin{frame}{Communications Metadata}
\protect\hypertarget{metadata}{}
\centering

\includegraphics[width=0.6\textwidth]{../arma-iu-2023/images/michael_hayden.jpg}

\begin{minipage}[b]{0.7\textwidth}
    \begin{quote}
        \small{``We Kill People Based on Metadata.''}

        \begin{flushright}
            \footnotesize{---Michael Hayden, former director, NSA.}
        \end{flushright}
    \end{quote}
\end{minipage}
\end{frame}

\input{../arma-iu-2023/anonymity-is.tex}

\input{../arma-iu-2023/three-hops.tex}

\begin{frame}
\begin{tikzpicture}[overlay, remember picture]

    \node[anchor=center] at (current page.center) {

        \begin{minipage}[c]{0.95\textwidth}

            \small{\alert{\textbf{I'm a political activist, part of a semi-criminalized minority.}} In my younger years I entered the public debate openly, and as a result got harassed by government agencies. I later tried to obfuscate my identity, but I found that my government has surprisingly broad powers to track down dissidents. \\

            Only by using anonymizing means, among which Tor is key, can I get my message out without having police come to "check my papers" in the middle of the night. \alert{\textbf{Tor allows me freedom to publish my message to the world without being personally persecuted for it.}}}

%            Being a dissident is hard enough, privacy is already heavily curtailed, so anonymized communication is a godsend.}
            \begin{flushright}

                \footnotesize{---Anonymous Tor User}

            \end{flushright}

        \end{minipage}

    };

\end{tikzpicture}
\end{frame}

\begin{frame}
\begin{tikzpicture}[overlay, remember picture]

    \node[anchor=center] at (current page.center) {

        \begin{minipage}[c]{0.95\textwidth}

            \small{\alert{\textbf{I'm a doctor in a very political town.}} I have patients who work on legislation that can mean billions of dollars to major telecom, social media, and search concerns. \\

            When I have to do research on diseases and treatment or look into aspects of my patients' histories, I am well aware that my search histories might be correlated to patient visits and leak information about their health, families, and personal lives. \alert{\textbf{I use Tor to do much of my research when I think there is a risk of correlating it to patient visits.}}}

            \begin{flushright}

                \footnotesize{---Anonymous Tor User}

            \end{flushright}

        \end{minipage}

    };
\end{tikzpicture}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{../arma-aisa-2024/images/bandwidth-2013-01-01-2023-11-28.png}
\end{figure}
\end{frame}

\begin{frame}{Tor's {\bf{safety}} comes from {\bf{diversity}}}

1. Diversity of relays. The more relays we have and the more diverse they
are, the fewer attackers are in a position to do traffic confirmation.

2. Diversity of users and reasons to use it. 50000 users in Iran means
almost all of them are normal citizens.

\end{frame}

\begin{frame}{Tor Browser}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{../arma-aisa-2024/images/130-homepage.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/ddg-pbm.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-aisa-2024/images/mr-incognito.jpg}
\end{figure}
\end{frame}

%\begin{frame}{Transparency for Tor is key}
%\begin{itemize}[label=\textbullet]
%\item<1-> Open source / free software
%\item<1-> Public design documents and specifications
%\item<1-> Publicly identified developers
%\item<2-> Not a contradiction: privacy is about choice!
%\end{itemize}
%\end{frame}

\begin{frame}{Ways to volunteer in the Tor network}

%Relay roles (ways to volunteer)

- Exit relays

- Non-exit relays

- Unlisted bridges (for censorship)

- Pluggable transport components like Snowflake

%from exit relays to
%non-exit relays to bridges to Snowflakes>

\end{frame}

\begin{frame}{Privacy is essential for freedom of learning}

At-risk populations, including in Europe, who need freedom from
tracking/surveillance in order to be comfortable participating on the
internet

\end{frame}

\begin{frame}{These values are part of larger discussions}

Privacy rights are tied to bigger societal decisions:

- data retention

- can citizens be trusted with end-to-end encryption

- should journalists be able to protect their sources

- internet blocking, commercial spyware, human rights

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\vspace{-1em}
\includegraphics[width=1.12\linewidth]{images/screenshot-relay-search-top-countries.png}
\end{figure}
\end{frame}

\begin{frame}{Outline}

\begin{itemize}
\item (0) Motivation / inspiration
\item (1) Intro to Tor and our values
\item {\bf{(2) Connection to educational missions, NRENs}}
%\item (3) Tor relays in European NRENs already
\item (3) Policy decisions you might face too
\end{itemize}

\end{frame}

% 2A, Start with Switch,
% - educational missions of NRENs and European universities and why
%   Tor is a good fit
% - what actually Switch hosts
% - what lessons you've learned / how it's actually less scary than
%   you might think.

\begin{frame}{Freedoms are in our mission}

{\LARGE{«Freedom of research\\\vspace{.1in}and teaching is guaranteed»}}

--Article 20, Federal Constitution of the Swiss Confederation

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/hackertarget-ch.png}
\end{figure}
\end{frame}

%\begin{frame}
%\begin{figure}[htbp]
%\includegraphics[width=1.1\linewidth]{images/screenshot-digitale-gesellschaft.png}
%\end{figure}
%\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.1\linewidth]{images/screenshot-digitale-gesellschaft-en.png}
\end{figure}
\end{frame}

%\begin{frame}{Abuse handling by the non-profit is better}
\begin{frame}{Abuse handling}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-whois.png}
\end{figure}
\end{frame}

\begin{frame}
%<funny illustration of letting other people do the work>
\begin{figure}[htbp]
\includegraphics[width=0.5\linewidth]{images/construction.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/switch-bandwidth.png}
\end{figure}
\end{frame}

%\begin{frame}{Switch benefits from Tor directly}

%We already use Tor daily in our work

%And our network security CERT people don't mind having the
%Netflow/(``anonymised'')PDNS data to compare against when investigating
%network attacks

%But of course please be careful with the data of at-risk citizens

%\end{frame}

\begin{frame}{Switch CERT use cases}

\vspace*{-4em}
\vbox to 0pt {
    \flushleft\includegraphics[width=0.50\textwidth]{images/netflow.jpg}
}
\vspace*{-4em}
\vbox to 0pt {
    \flushright\includegraphics[width=0.35\textwidth]{images/passive-dns.png}
}

\end{frame}

% 2B, And Switch isn't alone
\begin{frame}{And Switch isn't alone}

%NRENs in Netherlands, Sweden, Greece, Denmark, Croatia host
%Tor relays in a similar way to how Switch does it
SURF (Netherlands), SUNET (Sweden), GRNET (Greece), DEIC (Denmark),
CARNET (Croatia) host or run Tor relays in similar ways.

And individual universities in Austria, Belgium, England, Germany,
Netherlands, Romania, Sweden run relays too.

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{../arma-aisa-2024/images/eff-tor-university-challenge.png}
\end{figure}
\end{frame}

\begin{frame}{Why relays at universities?}
%<summarize and re-pitch the core arguments of EFF's campaign>

{\bf{Education}}: hands-on cyber-security experience; teaching students
about civil society; helping refine privacy advocacy skills.
% and getting them thinking about global policy, law, and society

\end{frame}


\begin{frame}{Why relays at universities?}
%<summarize and re-pitch the core arguments of EFF's campaign>

{\bf{Education}}: hands-on cyber-security experience; teaching students
about civil society; helping refine privacy advocacy skills.
% and getting them thinking about global policy, law, and society

{\bf{Community}}: Connecting student groups to professors; supporting freedom
of speech and freedom of learning; and increasing the capacity of the
Tor network.

\end{frame}

\begin{frame}{Why relays at universities?}
%<summarize and re-pitch the core arguments of EFF's campaign>

{\bf{Education}}: hands-on cyber-security experience; teaching students
about civil society; helping refine privacy advocacy skills.
% and getting them thinking about global policy, law, and society
\vspace{-.5em}

{\bf{Community}}: Connecting student groups to professors; supporting freedom
of speech and freedom of learning; and increasing the capacity of the
Tor network.
\vspace{-.5em}

{\bf{Research}}: Helping the Tor network stay strong so people can use it for
research;
attracting good students;
%giving research groups a recruiting ``competitive advantage''
%over their peer institutions;
giving researchers access to their own Tor relay.

\end{frame}

\begin{frame}{Outline}

\begin{itemize}
\item (0) Motivation / inspiration
\item (1) Intro to Tor and our values
\item (2) Connection to educational missions and NRENs
\item {\bf{(3) Policy decisions you might face too}}
\end{itemize}

\end{frame}

\begin{frame}{Policy/society decisions}

Tor relays in your country? Exit relays?

Tor relays hosted at your NREN? Run by your NREN staff?

Students in your country / at your university able to connect to the
Tor network?

Webservers in your country / at your university willing to serve content
to Tor users?

\end{frame}

\begin{frame}{Calls to action}

Understand the risks of centralized architectures

Participate in privacy / censorship research:
https://petsymposium.org/\\
https://foci.community/\\
both in Bristol (England) (+ virtual) in July

Help relays, bridges, and snowflakes in your country

Protect freedom of research, learning, and teaching

%https://donate.torproject.org/

\end{frame}

\end{document}

