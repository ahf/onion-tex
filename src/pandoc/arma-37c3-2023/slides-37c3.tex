%% Copyright (c) 2018 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,17pt]{beamer}

\usepackage{soul,comment}
\usepackage{bbding,enumitem}

\usepackage{tikzpeople}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{ulem}

\usetheme{onion}

\title{Tor censorship attempts in\\Russia, Iran, Turkmenistan}
%\subtitle{37c3}

\vspace{-1cm}
\date{December 28 2023}
\author{Roger Dingledine}
%\institute{The Tor Project}
\titlegraphic{\onionlogo{OnionBlack}}

\setlength{\parskip}{3mm}

% put page numbers on each slide
\setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

%% AHF: \tikzexternalize uses the external library to cache tikz images, but needs --shell-escape to be passed to pdflatex.
%\usetikzlibrary{shapes,backgrounds,dateplot,external}
\usetikzlibrary{shapes,backgrounds,dateplot,external,lindenmayersystems}
%%\tikzexternalize[prefix=tikz/]

\newcommand{\highlight}[1]{\textbf{\alert{#1}}}

\newcommand{\colorhref}[3][OnionBlack]{\href{#2}{\color{#1}{#3}} }

\begin{document}

\maketitle

\begin{frame}{Outline}

\begin{itemize}
\item {\bf{(1) Intro to Tor}}
\item (2) Intro to Tor and censorship resistance
\item (3) Russia
\item (4) Iran
\item (5) Turkmenistan
\item (6) Bigger context
\end{itemize}

\end{frame}

%%%%%%% (1) Intro to Tor

\begin{frame}{Tor Overview}

%Tor provides
Online anonymity: open source, open network

Community of devs, researchers, users, relay operators

US 501(c)(3) non-profit organization with 50ish staff

Estimated 2,000,000 to 8,000,000 daily users

Part of larger ecosystems: internet freedom, free software, censorship
resistance, anonymity research

\end{frame}

\iffalse
\begin{frame}{History}
\protect\hypertarget{history}{}
\begin{description}
%\item[\textbf{1990s}]
%Onion routing for privacy online.
\item[\textbf{2002--}]
Research with U.S.~Naval Research Lab.
\item[\textbf{2004}]
Sponsored by Electronic Frontier Foundation.
\item[\textbf{2006}]
The Tor Project, Inc.~became a non-profit.
\item[\textbf{2007}]
\highlight{Expansion to anti-censorship.}
\item[\textbf{2008}]
Tor Browser development.
\item[\textbf{2010}]
The Arab spring.
\item[\textbf{2013}]
The summer of Snowden.
\item[\textbf{2018}]
\highlight{Dedicated anti-censorship team created.}
\end{description}
\end{frame}
\fi

\input{threat-model.tex}

\begin{frame}{Communications Metadata}
\protect\hypertarget{metadata}{}
\centering

\includegraphics[width=0.6\textwidth]{images/michael_hayden.jpg}

\begin{minipage}[b]{0.7\textwidth}
    \begin{quote}
        \small{"We Kill People Based on Metadata."}

        \begin{flushright}
            \footnotesize{---Michael Hayden, former director, NSA}
        \end{flushright}
    \end{quote}
\end{minipage}
\end{frame}

\input{anonymity-is.tex}

\input{three-hops.tex}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/bandwidth-2013-01-01-2023-11-28.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/tor-browser-115.png}
\end{figure}
\end{frame}

\begin{frame}{Transparency for Tor is key}

\begin{itemize}[label=\textbullet]
\item<1-> Open source / free software
\item<1-> Public design documents and specifications
\item<1-> Publicly identified developers
\item<2-> Not a contradiction: privacy is about choice!
\end{itemize}

\end{frame}

%%%%%%% (2) Crash course on censorship-resistance

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item {\bf{(2) Intro to Tor and censorship resistance}}
\item (3) Russia
\item (4) Iran
\item (5) Turkmenistan
\item (6) Bigger context
\end{itemize}

\end{frame}

\begin{frame}{Bridges, for IP address blocking}

The Tor network is made up of 8000 public relays, but the list is public
and you can just fetch it and block them by IP address.

So, we have unlisted relays called ``bridges'' and there is a
cat-and-mouse game where users try to get a bridge that the censor didn't
already find.

\end{frame}

\begin{frame}{Pluggable transports, for DPI blocking}

%At the same time there is a \emph{protocol-level} arms race: the original
%Tor protocol looks mostly like TLS, but there are distinguishers if you
%look carefully enough.
{\it{Protocol-level}} arms race too: the Tor protocol looks mostly like
TLS, but only if you don't look too carefully.

%Rather than somehow perfectly mimicking a browser talking to a webserver,
%instead our ``pluggable transports'' design aims for modularity:
Rather than perfectly mimicking Firefox+Apache, our ``pluggable
transports'' design aims for modularity:

Tor's three-hop path provides the privacy, and
you can plug in different {\it{transports}} that transform the first
link's traffic into flows that your censor doesn't block.

\end{frame}

\begin{frame}{obfs4 pluggable transport}

obfs4 is still the core most successful transport.

It simply adds a new layer of encryption on top, so there are no
recognizable headers or formatting at the content layer.

The idea is that automated protocol classifiers won't have any good
guesses, so censors are forced to either block all unclassified traffic
or allow it all through.

\end{frame}

\begin{frame}{Snowflake pluggable transport}

Snowflake makes your traffic look like a WebRTC (zoom, jitsi, skype,
signal, etc) call, and those are allowed in many parts of the
world.

%Other key innovation is that 
People can volunteer as Snowflake
proxies simply by installing an extension in their browser.

The resulting volume and variety of volunteers gives us more options on
how to distribute them to users.

\end{frame}

\begin{frame}{meek pluggable transport}

%Tunnels traffic inside an https request to a shared cloud service
Domain fronting: Makes https request to a shared cloud service (azure,
fastly, etc), and tunnels traffic inside it

Outer layer says the SNI (Server Name Indicator) of a popular site, but
inner layer has a different Host: header

Have to pay cloud prices for the bandwidth :(, so not great for proxying
full traffic flows

\end{frame}

\begin{frame}{Matching bridges to users who need them}
%Distributors / brokers}

%Services for matching up bridges to users who need them.

Divide obfs4 bridges into distribution buckets, where each
bucket relies on a different scarce resource to rate-limit how many
bridges the censor can get.

\begin{itemize}[label=\textbullet]

\item https (get a few bridges based on your IPv4 /16)

\item gmail (get a few bridges based on your username)

\item moat (Tor Browser makes a domain-fronted connection, presents
a captcha in-browser, and auto-populates your bridge settings).

\end{itemize}

\end{frame}
\begin{frame}{Matching bridges to users who need them}

and Snowflake has a similar ``broker'' service that matches up Snowflake
users to Snowflake volunteers.

\end{frame}

%%%%%%% (3) Russia

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item \sout{(2) Intro to Tor and censorship resistance}
\item {\bf{(3) Russia}}
\item (4) Iran
\item (5) Turkmenistan
\item (6) Bigger context
\end{itemize}

\end{frame}

\input{russia.tex}

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item \sout{(2) Intro to Tor and censorship resistance}
\item \sout{(3) Russia}
\item {\bf{(4) Iran}}
\item (5) Turkmenistan
\item (6) Bigger context
\end{itemize}

\end{frame}

\input{iran.tex}

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item \sout{(2) Intro to Tor and censorship resistance}
\item \sout{(3) Russia}
\item \sout{(4) Iran}
\item {\bf{(5) Turkmenistan}}
\item (6) Bigger context
\end{itemize}

\end{frame}

\input{turkmenistan.tex}

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item \sout{(2) Intro to Tor and censorship resistance}
\item \sout{(3) Russia}
\item \sout{(4) Iran}
\item \sout{(5) Turkmenistan}
\item {\bf{(6) Bigger context}}
\end{itemize}

\end{frame}

\begin{frame}{Other surprises}

\begin{itemize}[label=\textbullet]

\item rt.com censored from many Tor exits, because Europe (i.e. France,
part of Germany, maybe more now?) pledged to censor it.

\item Actually, many Tor exits can't reach sites in Russia now, because
the blocking is bidirectional?

\item New groups of Russian and Ukrainian exit relays, ``hmm''

%\item I'm talking to a contact at Amazon about pitching turning back on domain fronting... for Ukraine

\end{itemize}

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/snowflake-locations2.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/userstats-relay-country-ua-2021-12-24-2022-03-24-off.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/russia-sanctions.png}
\end{figure}
\end{frame}

\begin{frame}{First, a rant about sanctions}

Especially about hurting internet connectivity for people in Russia as
a way to punish their government.

Compare to the effects of Trump's ``maximum pressure'' sanctions
against Iran.

We will see the same outcome in Russia.

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-rt.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.7\textwidth]{images/jealous-meme.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.6\textwidth]{images/offramp-meme.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{images/office-meme.jpg}
\end{figure}
\end{frame}

\begin{frame}

\huge{\textcolor{red}{WTF Europe?? Why u like censoring so much??}}

\end{frame}

\begin{frame}{Internet censorship: early warning system}

Notice that our Russia Tor blocking story started at the beginning of
December 2021.

From the rest of the world's perspective, the Russia story started in
February 2022.

So (a) yeah they knew this was coming, and (b) internet censorship often
serves as an early warning system for upcoming political events.

\end{frame}

\iffalse
\begin{frame}{GFW blocking by too much entropy}

China is experimenting with blocking based on flow-level entropy: if
the first k characters in the flow are too random, they kill it.

Only doing it to flows destined for certain cheapo providers like Hetzner,
OVH, Digital Ocean, Alibaba.

Maybe they can't tolerate the collateral damage from doing it more
widely? Or, maybe they can?

We're going to need better transports.

\end{frame}
\fi

\begin{frame}{Calls to action}

Please run bridges!

Please run snowflakes!

Please run relays!

...How do we fix policy in these countries?

Please participate in anti-censorship research!
https://foci.community/ attached to https://petsymposium.org (in Bristol in July).

\end{frame}

\begin{frame}{Calls to action}

(Run bridges, snowflakes, relays! Fix policy! Research!)

Day 3 Tor spaces:\\
- general meetup (Saal E 16:00-18:00)\\
- relay operators meetup and Q\&A (Saal D 20:30-22:00)\\
- torservers.net meetup (Saal D 00:00-01:30)

\end{frame}

\end{document}

