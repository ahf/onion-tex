%% Copyright (c) 2018 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,17pt]{beamer}

\usepackage{soul,comment}
\usepackage{bbding,enumitem}

\usepackage{tikzpeople}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{ulem}

%\usepackage[export]{adjustbox}

\usetheme{onion}

\title{Tor OSIRIS Hack Night}

\date{March 7 2024}
\author{Roger Dingledine, The Tor Project}
%\institute{The Tor Project}
%\vspace{-1cm}
\titlegraphic{\onionlogo{OnionBlack}}

\setlength{\parskip}{3mm}

% put page numbers on each slide
\setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

%% AHF: \tikzexternalize uses the external library to cache tikz images, but needs --shell-escape to be passed to pdflatex.
%\usetikzlibrary{shapes,backgrounds,dateplot,external}
\usetikzlibrary{shapes,backgrounds,dateplot,external,lindenmayersystems}
%%\tikzexternalize[prefix=tikz/]

\newcommand{\highlight}[1]{\textbf{\alert{#1}}}

\newcommand{\colorhref}[3][OnionBlack]{\href{#2}{\color{#1}{#3}} }

\begin{document}

\maketitle

\begin{frame}{Tor Overview}

%Tor provides
Online anonymity: open source, open network

Community of devs, researchers, users, relay operators

US 501(c)(3) non-profit organization with 50ish staff

Estimated 2,000,000 to 8,000,000 daily users

Part of larger ecosystems: internet freedom, free software, censorship
resistance, anonymity research

\end{frame}

\input{../arma-iu-2023/threat-model.tex}

\begin{frame}{Communications Metadata}
\protect\hypertarget{metadata}{}
\centering

\includegraphics[width=0.6\textwidth]{../arma-iu-2023/images/michael_hayden.jpg}

\begin{minipage}[b]{0.7\textwidth}
    \begin{quote}
        \small{"We Kill People Based on Metadata."}

        \begin{flushright}
            \footnotesize{---Michael Hayden, former director, NSA.}
        \end{flushright}
    \end{quote}
\end{minipage}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/bill-barr.png}
\end{figure}
\end{frame}

\input{../arma-iu-2023/anonymity-is.tex}

\input{../arma-iu-2023/three-hops.tex}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/bandwidth-2013-01-01-2023-11-28.png}
\end{figure}
\end{frame}

\begin{frame}{Tor's {\bf{safety}} comes from {\bf{diversity}}}

1. Diversity of relays. The more relays we have and the more diverse they
are, the fewer attackers are in a position to do traffic confirmation.
Research problem: How do we measure diversity over time?

2. Diversity of users and reasons to use it. 50000 users in Iran means
almost all of them are normal citizens.

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/tor-browser-115.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/ddg-pbm.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/mr-incognito.jpg}
\end{figure}
\end{frame}


\begin{frame}{Transparency for Tor is key}

\begin{itemize}[label=\textbullet]
\item<1-> Open source / free software
\item<1-> Public design documents and specifications
\item<1-> Publicly identified developers
\item<2-> Not a contradiction: privacy is about choice!
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%% Onion Services

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/onion-services.png}
\end{figure}
\end{frame}

\begin{frame}{Onion service properties}

\begin{itemize}[label=\textbullet]
\item Self authenticated
\item End-to-end encrypted
\item Built-in NAT punching
\item Limit surface area
\item No need to ``exit'' from Tor
\end{itemize}

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/zyprexa.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.18\textwidth]{images/wired.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{../arma-iu-2023/images/iceberg.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{../arma-iu-2023/images/facebook_onion.png}
\includegraphics[width=\linewidth]{../arma-iu-2023/images/facebook_tor_news.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/bbc-onion.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{../arma-iu-2023/images/cloudflare-onion.png}
\end{figure}
\end{frame}

\begin{frame}{OnionShare}
\vspace{-0.25in}
\begin{figure}[t]
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../arma-iu-2023/images/onionshare1.png}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../arma-iu-2023/images/onionshare2.png}
\end{minipage}
\end{figure}

\end{frame}

\begin{frame}{Ricochet}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{../arma-iu-2023/images/ricochet.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.1\textwidth]{images/bitcoin-onion-services.png}
\end{figure}
\end{frame}

\input{../arma-iu-2023/onion-venn.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/tor-onion-services-1.png}
\end{figure}
\end{frame}
\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/tor-onion-services-2.png}
\end{figure}
\end{frame}
\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/tor-onion-services-3.png}
\end{figure}
\end{frame}
\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/tor-onion-services-4.png}
\end{figure}
\end{frame}
\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/tor-onion-services-5.png}
\end{figure}
\end{frame}
\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/tor-onion-services-6.png}
\end{figure}
\end{frame}

\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/hashring.png}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Flaw: HSDir relays were too predictable}

The six daily HSDirs for a given onion address were predictable into
the future.

So a bad guy could run six relays with just the right keys to target a
specific future day... to censor or to measure popularity.

A few people---we don't know who---were doing this attack in practice.

\end{frame}

\begin{frame}{Fix: Global shared random value}

Make the HSDir mapping include a communal random value that everybody
agrees about but that nobody can predict.

The directory authorities pick this value each day as part of their
consensus voting process.

\end{frame}

\begin{frame}{Flaw: HSDirs got to learn onion addresses}

The onion service descriptor (which gets uploaded to the HSDir) included
the public key for the service (so everybody can check the signature).

So you could run relays and discover otherwise-unpublished onion addresses.

``Threat intelligence'' companies were trying to do just that.

\end{frame}

\begin{frame}{Fix: New crypto hides the address}

The new ed25519 cryptosystem has a cool feature where you can sign the
onion descriptor with a subkey.

So everybody can check the signature but nobody can learn the main key
from the subkey or signature.

Should finally kill the arms race with jerks running relays to gather onions.

\end{frame}

\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=1.1\textwidth]{images/ntro.png}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/tor-onion-services-4.png}
\end{figure}
\end{frame}

\begin{frame}{You can send many many introductions}

For example, if you want to deny service to a target onion service,
spin up millions of Tor clients and have each of them try to visit it.

It's an anonymity system (!) so the service can't tell which intro cells
to drop and which to answer.

\end{frame}

\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/userstats-relay-country-us-2023-09-07-2024-01-06.png}
\end{figure}
\end{frame}

\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/hidserv-rend-v3-relayed-cells-2023-09-07-2024-01-06.png}
\end{figure}
\end{frame}

\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/torperf-1mb-2023-09-07-2024-01-06.png}
\end{figure}
\end{frame}

\begin{frame}{Fix: integrate PoW into the intro cell}

Onion services, when under attack, advertise a suggested effort level
in their onion descriptor.

Clients see the effort level and bid for attention (``blind auction'' in
economics terms).

The service side sorts the incoming intro cells, then answers best effort.

\end{frame}

\begin{frame}{PoW in the intro cell}

Service should follow a feedback loop: raise effort if it can't keep up,
lower effort if it is keeping up.

The PoW we've picked aims to not be FPGA/ASIC friendly.

Still some messy design questions, e.g.~at what rate (how many) should
we try to answer?

\end{frame}

\begin{frame}{But what about...}

Doesn't this favor desktops over mobiles?

I thought PoW/bitcoin destroyed the climate?

Why do you want to help {\it{those}} onion services?

\end{frame}

\begin{frame}{}
\begin{figure}[htbp]
\includegraphics[width=1.1\textwidth]{images/prop327.png}
\end{figure}
\end{frame}


%%%%%%%%%%%%%%% Wrap up

\begin{frame}{Tor isn't foolproof}

\begin{itemize}[label=\textbullet]
\item Opsec mistakes
\item Browser metadata fingerprints
\item Browser exploits
\item Traffic analysis
\end{itemize}

\end{frame}

\begin{frame}{Calls to action}

Understand the risks of centralized architectures

Fight the slippery slope that is internet censorship

Participate in privacy / censorship research, e.g.
https://petsymposium.org/\\
https://foci.community/

Volunteer as a relay, a bridge, or a snowflake!

%https://donate.torproject.org/

\end{frame}

\end{document}

