%% Copyright (c) 2018 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,17pt]{beamer}

\usepackage{soul,comment}
\usepackage{bbding,enumitem}

\usepackage{tikzpeople}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{ulem}

%\usepackage[export]{adjustbox}

\usetheme{onion}

\title{Tor: Internet privacy in the age of big surveillance}

%\date{April 7 2023}
\date{}
\author{Roger Dingledine, The Tor Project}
%\institute{The Tor Project}
%\vspace{-1cm}
\titlegraphic{\onionlogo{OnionBlack}}

\setlength{\parskip}{3mm}

% put page numbers on each slide
\setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

%% AHF: \tikzexternalize uses the external library to cache tikz images, but needs --shell-escape to be passed to pdflatex.
%\usetikzlibrary{shapes,backgrounds,dateplot,external}
\usetikzlibrary{shapes,backgrounds,dateplot,external,lindenmayersystems}
%%\tikzexternalize[prefix=tikz/]

\newcommand{\highlight}[1]{\textbf{\alert{#1}}}

\newcommand{\colorhref}[3][OnionBlack]{\href{#2}{\color{#1}{#3}} }

\begin{document}

\maketitle

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.2\textwidth]{images/screenshot-conference-program.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.2\textwidth]{images/screenshot-aisa.png}
\end{figure}
\end{frame}

\begin{frame}{Tor Overview}

%Tor provides
Online anonymity: open source, open network

Community of devs, researchers, users, relay operators

US 501(c)(3) non-profit organization with 50ish staff

Estimated 2,000,000 to 8,000,000 daily users

Part of larger ecosystems: internet freedom, free software, censorship
resistance, anonymity research

\end{frame}

\input{../arma-iu-2023/threat-model.tex}

\begin{frame}{Communications Metadata}
\protect\hypertarget{metadata}{}
\centering

\includegraphics[width=0.6\textwidth]{../arma-iu-2023/images/michael_hayden.jpg}

\begin{minipage}[b]{0.7\textwidth}
    \begin{quote}
        \small{"We Kill People Based on Metadata."}

        \begin{flushright}
            \footnotesize{---Michael Hayden, former director, NSA.}
        \end{flushright}
    \end{quote}
\end{minipage}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{../arma-iu-2023/images/bill-barr.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{images/screenshot-uk-meta-e2e.png}
\end{figure}
\end{frame}

\input{../arma-iu-2023/anonymity-is.tex}

\input{../arma-iu-2023/three-hops.tex}

\begin{frame}
\begin{tikzpicture}[overlay, remember picture]

    \node[anchor=center] at (current page.center) {

        \begin{minipage}[c]{0.95\textwidth}

            \small{\alert{\textbf{I'm a political activist, part of a semi-criminalized minority.}} In my younger years I entered the public debate openly, and as a result got harassed by government agencies. I later tried to obfuscate my identity, but I found that my government has surprisingly broad powers to track down dissidents. \\

            Only by using anonymizing means, among which Tor is key, can I get my message out without having police come to "check my papers" in the middle of the night. \alert{\textbf{Tor allows me freedom to publish my message to the world without being personally persecuted for it.}}}

%            Being a dissident is hard enough, privacy is already heavily curtailed, so anonymized communication is a godsend.}
            \begin{flushright}

                \footnotesize{---Anonymous Tor User}

            \end{flushright}

        \end{minipage}

    };

\end{tikzpicture}
\end{frame}

\begin{frame}
\begin{tikzpicture}[overlay, remember picture]

    \node[anchor=center] at (current page.center) {

        \begin{minipage}[c]{0.95\textwidth}

            \small{\alert{\textbf{I'm a doctor in a very political town.}} I have patients who work on legislation that can mean billions of dollars to major telecom, social media, and search concerns. \\

            When I have to do research on diseases and treatment or look into aspects of my patients' histories, I am well aware that my search histories might be correlated to patient visits and leak information about their health, families, and personal lives. \alert{\textbf{I use Tor to do much of my research when I think there is a risk of correlating it to patient visits.}}}

            \begin{flushright}

                \footnotesize{---Anonymous Tor User}

            \end{flushright}

        \end{minipage}

    };
\end{tikzpicture}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/bandwidth-2013-01-01-2023-11-28.png}
\end{figure}
\end{frame}

\begin{frame}{Tor's {\bf{safety}} comes from {\bf{diversity}}}

1. Diversity of relays. The more relays we have and the more diverse they
are, the fewer attackers are in a position to do traffic confirmation.
Research problem: How do we measure diversity over time?

2. Diversity of users and reasons to use it. 50000 users in Iran means
almost all of them are normal citizens.

\end{frame}

\begin{frame}{Tor Browser}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/130-homepage.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/ddg-pbm.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/mr-incognito.jpg}
\end{figure}
\end{frame}


\begin{frame}{Transparency for Tor is key}

\begin{itemize}[label=\textbullet]
\item<1-> Open source / free software
\item<1-> Public design documents and specifications
\item<1-> Publicly identified developers
\item<2-> Not a contradiction: privacy is about choice!
\end{itemize}

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/ed-tor.png}
\end{figure}
\end{frame}

%\begin{frame}
%\begin{figure}[htbp]
%\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/ola-bini.png}
%\end{figure}
%\end{frame}

\begin{frame}{Technology is not neutral}

Power imbalances are central to Tor: it's more useful to
vulnerable people than those who already have power.

Technology is inherently political!

%But that doesn't mean we need to like or endorse or talk about all of our
%users. `Absolute free speech' vs `framing and community-building' vs
%`how the system works technically'.

\end{frame}

%%%%%%%%%%%%%%% Censorship

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{../arma-iu-2023/images/block-result.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{../arma-iu-2023/images/pluggable-transports.png}
\end{figure}
\end{frame}

\begin{frame}{Snowflake pluggable transport}

Snowflake makes your traffic look like a WebRTC (zoom, jitsi, skype,
signal, etc) call, and those are allowed in many parts of the
world.

People can volunteer as Snowflake
proxies simply by installing an extension in their browser.

The resulting volume and variety of volunteers gives us more options on
how to distribute them to users.

\end{frame}

\begin{frame}{Daily snowflake users}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/snowflake-users-russia.png}
\end{figure}
\end{frame}


\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{../arma-iu-2023/images/snowflake-big-picture.png}
\end{figure}
\end{frame}

\iffalse

\begin{frame}{First, a rant about sanctions}

Especially about hurting internet connectivity for people in Russia as
a way to punish their government.

Compare to the effects of Trump's ``maximum pressure'' sanctions
against Iran.

We will see the same outcome in Russia.

\end{frame}

\fi

\begin{frame}{Arms races}

Censorship arms race is bad

Surveillance arms race is worse

...and centralization of the internet makes it worse still

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/china-censors.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/east-turkestan.png}
\end{figure}
\end{frame}

\begin{frame}{Censorship implies surveillance}

Recurring theme where western companies build the oppression tools

Censorship becoming more widespread in the west (e.g. RT.com in europe)

%Iran's ``halal internet'' dreams, Russia heading there too

over-there-istan

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/gv-tunisia.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.1\textwidth]{../arma-iu-2023/images/russia-sanctions.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.1\textwidth]{images/screenshot-australia-censorship.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.1\textwidth]{images/screenshot-security-bill.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.1\textwidth]{images/screenshot-youtube-data.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{images/facebook-abortion-turnover.png}
\end{figure}
\end{frame}


%%%%%%%%%%%%%%% Onion Services

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/onion-services.png}
\end{figure}
\end{frame}

\begin{frame}{Onion service properties}

\begin{itemize}[label=\textbullet]
\item Self authenticated
\item End-to-end encrypted
\item Built-in NAT punching
\item Limit surface area
\item No need to ``exit'' from Tor
\end{itemize}

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{../arma-iu-2023/images/iceberg.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{../arma-iu-2023/images/iceberg-skull.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{../arma-iu-2023/images/facebook_onion.png}
\includegraphics[width=\linewidth]{../arma-iu-2023/images/facebook_tor_news.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{../arma-iu-2023/images/bbc-onion.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{../arma-iu-2023/images/cloudflare-onion.png}
\end{figure}
\end{frame}

\input{../arma-iu-2023/onion-venn.tex}

\begin{frame}{OnionShare}
\vspace{-0.25in}
\begin{figure}[t]
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../arma-iu-2023/images/onionshare1.png}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../arma-iu-2023/images/onionshare2.png}
\end{minipage}
\end{figure}

\end{frame}

\begin{frame}{Ricochet}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{../arma-iu-2023/images/ricochet.png}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%% Wrap up

\begin{frame}{Tor isn't foolproof}

\begin{itemize}[label=\textbullet]
\item Opsec mistakes
\item Browser metadata fingerprints
\item Browser exploits
\item Traffic analysis
\end{itemize}

\end{frame}

\begin{frame}{The crypto wars}

We should take as many lessons as we can from the https win.

If you hear that the NSA stores encrypted traffic for longer, what do you do?

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{images/screenshot-iphone-backdoor.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.9\textwidth]{images/eff-tor-university-challenge.png}
\end{figure}
\end{frame}

\begin{frame}{Calls to action}

Understand the risks of centralized architectures

Recognize the conflict between your foreign policy goals and your domestic
censorship hopes

Participate in privacy / censorship research, e.g.
https://petsymposium.org/\\
https://foci.community/

Volunteer as a relay, a bridge, or a snowflake!

%https://donate.torproject.org/

\end{frame}

\end{document}

\begin{frame}{Want even more info?}

Defcon 2022 talk: ``How Russia is trying to block Tor''

Defcon 2017 talk: ``Next Generation Tor Onion Services''

\end{frame}

